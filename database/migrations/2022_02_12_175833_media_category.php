<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class MediaCategory extends Migration
{
    public function up(): void
    {
        Schema::create('media_category', function (Blueprint $table) {
            $table->id();
            $table->foreignId('media_id')->constrained('media');
            $table->foreignId('category_id')->constrained('categories');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('media_category');
    }
}
