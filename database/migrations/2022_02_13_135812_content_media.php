<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ContentMedia extends Migration
{
    public function up(): void
    {
        Schema::create('content_media', function (Blueprint $table) {
            $table->id();
            $table->foreignId('media_id')->constrained('media');
            $table->foreignId('content_id')->constrained('contents');
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('content_media');
    }
}
