<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBlogsTable extends Migration
{
    public function up(): void
    {
        Schema::create('blogs', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('lang_id')->nullable()->constrained('langs');
            $table->foreignId('media_id')->nullable()->constrained('media');
            $table->foreignId('category_id')->nullable()->constrained('categories');
            $table->string('title')->nullable();
            $table->text('description')->nullable();
            $table->boolean('activate')->comment('zobrazenie príspevku');
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('blogs');
    }
}
