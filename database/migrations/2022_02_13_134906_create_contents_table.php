<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentsTable extends Migration
{
    public function up(): void
    {
        Schema::create('contents', function (Blueprint $table) {
            $table->id();
            $table->uuid('uuid');
            $table->foreignId('blog_id')->constrained('blogs');
            $table->text('description')->nullable();
            $table->text('sub_description')->nullable();
            $table->string('title')->nullable();
            $table->string('sub_title')->nullable();
            $table->enum('type',['parameter','detail'])->nullable();
            $table->timestamps();
        });
    }

    public function down(): void
    {
        Schema::dropIfExists('contents');
    }
}
