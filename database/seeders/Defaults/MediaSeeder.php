<?php

namespace Database\Seeders\Defaults;

use App\Models\Media;
use Illuminate\Database\Seeder;

class MediaSeeder extends Seeder
{
    public function run(): void
    {
        Media::query()->create([
            'type' => 'portfolio',
            'alt' => 'exlink',
            'name' => 'work-1.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'fimak',
            'name' => 'work-2.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'mkcore',
            'name' => 'work-3.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'roche',
            'name' => 'work-4.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'tonprint',
            'name' => 'work-6.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'kremik',
            'name' => 'work-7.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'pijeme',
            'name' => 'work-8.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'rs-tech',
            'name' => 'work-9.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'pizza-pod-agacom',
            'name' => 'work-10.png',
            'path' => 'works',
            'header' => 'image/png',
        ])->create([
            'type' => 'portfolio',
            'alt' => 'faberlicnassvet',
            'name' => 'work-11.png',
            'path' => 'works',
            'header' => 'image/png',
        ]);
    }
}
