<?php

namespace Database\Seeders\Defaults;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    public function run(): void
    {
        Category::query()
            ->create([
                'type' => 'portfolio',
                'filter' => '.all',
                'name' => 'all',
                'description' => 'selected',
            ])
            ->create([
                'type' => 'portfolio',
                'filter' => '.system',
                'name' => 'system',
                'description' => null,
            ])->create([
                'type' => 'portfolio',
                'filter' => '.cms',
                'name' => 'cms',
                'description' => null,
            ])->create([
                'type' => 'portfolio',
                'filter' => '.presentation',
                'name' => 'presentation',
                'description' => null,
            ])->create([
                'type' => 'portfolio',
                'filter' => '.freelancer',
                'name' => 'freelancer',
                'description' => null,
            ])->create([
                'type' => 'portfolio',
                'filter' => '.team',
                'name' => 'team',
                'description' => null,
            ])->create([
                'type' => 'portfolio',
                'filter' => '.eshop',
                'name' => 'eshop',
                'description' => null,
            ])->create([
                'type' => 'blog',
                'filter' => 'all',
                'name' => 'projekty',
                'description' => null,
            ]);
    }
}
