<?php

namespace Database\Seeders\Defaults;

use App\Models\Lang;
use Illuminate\Database\Seeder;

class LangSeeder extends Seeder
{
    public function run(): void
    {
        Lang::query()->create([
            'code' => 'SK',
            'name' => 'Slovak',
            'icon' => null,
        ])->create([
            'code' => 'EN',
            'name' => 'English',
            'icon' => null,
        ]);
    }
}
