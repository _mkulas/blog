<?php

namespace Database\Seeders\Defaults;

use App\Models\Blog;
use Illuminate\Database\Seeder;

class BlogSeeder extends Seeder
{
    public function run(): void
    {
        Blog::query()->create([
            'lang_id' => 1,
            'media_id' => 1,
            'category_id' => 8,
            'title' => 'Exlink',
            'description' => 'Scheduler ktorý spája expertov .',
            'activate' => true,
        ])->create([
            'lang_id' => 2,
            'media_id' => 1,
            'category_id' => 8,
            'title' => 'Exlink',
            'description' => 'EN',
            'activate' => true,
        ])->create([
            'lang_id' => 1,
            'media_id' => 2,
            'category_id' => 8,
            'title' => 'Firmak',
            'description' => 'Interný systém pre správu dokumentov .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 2,
            'category_id' => 8,
            'title' => 'Firmak',
            'description' => 'EN',
            'activate' => false,
        ])->create([
            'lang_id' => 1,
            'media_id' => 3,
            'category_id' => 8,
            'title' => 'MKCORE',
            'description' => 'Administrácia pre prezenčné weby a eshopy .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 3,
            'category_id' => 8,
            'title' => 'MKCORE',
            'description' => 'EN',
            'activate' => false,
        ])->create([
            'lang_id' => 1,
            'media_id' => 4,
            'category_id' => 8,
            'title' => 'ROCHE',
            'description' => 'Evidencia a dohľadanie špecialistov.',
            'activate' => true,
        ])->create([
            'lang_id' => 2,
            'media_id' => 4,
            'category_id' => 8,
            'title' => 'ROCHE',
            'description' => 'EN',
            'activate' => true,
        ])->create([
            'lang_id' => 1,
            'media_id' => 5,
            'category_id' => 8,
            'title' => 'TONPRINT',
            'description' => 'Eshop , všetko pre vaše tlačiarne .',
            'activate' => true,
        ])->create([
            'lang_id' => 2,
            'media_id' => 5,
            'category_id' => 8,
            'title' => 'TONPRINT',
            'description' => 'EN',
            'activate' => true,
        ])->create([
            'lang_id' => 1,
            'media_id' => 6,
            'category_id' => 8,
            'title' => 'Kremik',
            'description' => 'Eshop , s bohatým sortimentom .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 6,
            'category_id' => 8,
            'title' => 'Kremik',
            'description' => 'E',
            'activate' => false,
        ])->create([
            'lang_id' => 1,
            'media_id' => 7,
            'category_id' => 8,
            'title' => 'Pijeme',
            'description' => 'Eshop , s ktorým nikdy nebudete smädný .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 7,
            'category_id' => 8,
            'title' => 'Pijeme',
            'description' => 'EN',
            'activate' => false,
        ])->create([
            'lang_id' => 1,
            'media_id' => 8,
            'category_id' => 8,
            'title' => 'Rs-tech',
            'description' => 'Služba opravy elektoniky rôzného druhu .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 8,
            'category_id' => 8,
            'title' => 'Rs-tech',
            'description' => 'EN',
            'activate' => false,
        ])->create([
            'lang_id' => 1,
            'media_id' => 9,
            'category_id' => 8,
            'title' => 'Pizza pod agačom',
            'description' => 'Chutná pizza s ktorou nebudete hladný .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 9,
            'category_id' => 8,
            'title' => 'Pizza pod agačom',
            'description' => 'En',
            'activate' => false,
        ])->create([
            'lang_id' => 1,
            'media_id' => 10,
            'category_id' => 8,
            'title' => 'Faberlicnassvet',
            'description' => 'Všetko pre vašu krásu a čistú domácnosť .',
            'activate' => false,
        ])->create([
            'lang_id' => 2,
            'media_id' => 10,
            'category_id' => 8,
            'title' => 'Faberlicnassvet',
            'description' => 'EN',
            'activate' => false,
        ]);
    }
}
