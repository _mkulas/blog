<?php

namespace Database\Seeders\Defaults;

use App\Models\Content;
use Illuminate\Database\Seeder;

class ContentSeeder extends Seeder
{
    public function run(): void
    {
        Content::query()->create([
            // exlink SK
            'blog_id' => 1,
            'description' => 'Exlink je platená celosvetová aplikácia ktorá sprája customerov z expertmy. ',
            'sub_description' => 'Na exlinku som pracoval ako backend z časti aj frontend programátor .
             Mojou úlohou bolo vytvoriť verziu dva , navrhovať štruktúru kódu , implementovať služby tretích strán ako STRIPE , CRONOFY , ZOOM . ',
            'title' => 'Exlink',
            'sub_title' => 'interný systém',
            'type' => null,
        ])->create([
            // Exlink EN
            'blog_id' => 2,
            'description' => 'Anglická verzia ',
            'sub_description' => 'Na exlinku som pracoval ako backend z časti aj frontend programátor .
             Mojou úlohou bolo vytvoriť verziu dva , navrhovať štruktúru kódu , implementovať služby tretích strán ako STRIPE , CRONOFY , ZOOM . ',
            'title' => 'Exlink',
            'sub_title' => 'interný systém',
            'type' => null,
        ])
            // firmak SK
            ->create([
                'blog_id' => 3,
                'description' => 'BookSystem je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'BookSystem',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])->create([
                'blog_id' => 4,
                // Firmak EN
                'description' => 'Anglická BookSystem je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'BookSystem',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])
            // mkcore sk
            ->create([
                'blog_id' => 5,
                'description' => 'MKCORE - cms systém pre komplexnú správu a administráciu prezenčných webov .',
                'sub_description' => 'Aplikácia sa stará o podrobný prehľad návšetevností , akcii , editáciu a komplexnú správu prezenčných webov , ktoré som vyvíjal ja ako freelancer.',
                'title' => 'MKCORE',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])->create([
                // mkcore EN
                'blog_id' => 6,
                'description' => 'Anglická BookSystem je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'BookSystem',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])
            // roche SK
            ->create([
                'blog_id' => 7,
                'description' => 'Roche je aplikácia pre správu interných zamestnancov spoločnosti .',
                'sub_description' => 'Aplikácia slúži na evidenciu , dostupnosť a dohľadanie doktorov - odborníkov na dopytujúce operácie či iné zákroky podľa aktuálny požiadaviek .',
                'title' => 'ROCHE',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])->create([
                // roche EN
                'blog_id' => 8,
                'description' => 'Anglická roche je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'ROCHE',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])->create([
                // tonprint SK
                'blog_id' => 9,
                'description' => 'Tonprint je verejne dostupný eshop v produkcii .',
                'sub_description' => 'Eshop sa venuje predávaniu tonerov pre rôzne tlačiarne v rôznych kategóriach . Beží na jednom multicore jadre spolu s kremikom a ďalšími inými ehopami .',
                'title' => 'Tonprint',
                'sub_title' => 'eshop',
                'type' => null,
            ])->create([
                // tonprint EN
                'blog_id' => 10,
                'description' => 'Anglická tonprint je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'Tonprint',
                'sub_title' => 'eshop',
                'type' => null,
            ])->create([
                // kremik SK
                'blog_id' => 11,
                'description' => 'Kremik je verejne dostupný eshop v produkcii . ',
                'sub_description' => 'Eshop sa venuje predávaniu rôzneho sortimentu. Beží na jednom multicore jadre spolu s kremikom a ďalšími inými ehopami .',
                'title' => 'Kremik',
                'sub_title' => 'eshop',
                'type' => null,
            ])->create([
                // kremik EN
                'blog_id' => 12,
                'description' => 'Anglická kremik je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'Kremik',
                'sub_title' => 'eshop',
                'type' => null,
            ])->create([
                // pijeme sk
                'blog_id' => 13,
                'description' => 'Pijeme bol verejne dostupný eshop na dovoz alkoholu . ',
                'sub_description' => 'Eshop sa staral o dodanie alkoholu pri rôznych akciách v čase ( nočných hodinách ) kedy už nebolo možné zakúpiť alkohol vo velkosklade či potravinách .',
                'title' => 'Pijeme',
                'sub_title' => 'eshop',
                'type' => null,
            ])->create([
                // pijeme EN
                'blog_id' => 14,
                'description' => 'Anglická pijeme je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'Pijeme',
                'sub_title' => 'eshop',
                'type' => null,
            ])->create([
                // rs tech sk
                'blog_id' => 15,
                'description' => 'Je prezenčná stránka ktorá čaká na uvedenie na trh . ',
                'sub_description' => 'Stránka má informačný charakter o poskytovaní služieb opravy zariadení . Jej jadro je pripravené na rozrastnutie sa v podobe eshopu. Beží na mccore jadre ',
                'title' => 'Rs-tech',
                'sub_title' => 'prezenčný web',
                'type' => null,
            ])->create([
                // rs tech EN
                'blog_id' => 16,
                'description' => 'RS TECH EN je prezenčný web s ponukou. Web je určený pre mobilné telefóny .',
                'sub_description' => 'Jadro je pripravené na rozrastenie sa v podobe eshopu a logistického systému pre dodávku pizze .',
                'title' => 'Pizza pod agacom',
                'sub_title' => 'prezenčný web pre mobily',
                'type' => null,
            ])->create([
                // Pizza pod agacom' sk
                'blog_id' => 17,
                'description' => 'Pizzeria pod agacom je prezenčný web s ponukou určený pre mobilné telefóny .',
                'sub_description' => 'Jadro je pripravené na rozrastenie sa v podobe eshopu a logistického systému pre dodávku pizze . Beží na mccore jadre .',
                'title' => 'Pizza pod agacom',
                'sub_title' => 'prezenčný web pre mobily',
                'type' => null,
            ])->create([
                // mkcore EN
                'blog_id' => 18,
                'description' => 'Anglická BookSystem je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Systém sa stará o predbežné účtovné prehľady spoločnosti , obsahuje vyše 1000 rôznych funkcii a je robený bez dokumentácie . Napája sa na rôzne služby tretích strán ako je finstat.sk a iné ...',
                'title' => 'BookSystem',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])->create([
                // mkcore sk
                'blog_id' => 19,
                'description' => 'Faberlicnassvet kozmetický alalytický systém .',
                'sub_description' => 'Aplikácia sa stará o správu registrácie pre mlm marketing . Automatické vyhodnocovanie kampane , plánovanie a komplexný prehľad o registrovaných užívateľov analytických dát predaja .',
                'title' => 'Faberlicnassvet',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ])->create([
                // mkcore EN
                'blog_id' => 20,
                'description' => 'Anglická BookSystem je interný systém ktorý nieje dostupná z vonku . ',
                'sub_description' => 'Aplikácia sa stará o správu registrácie pre mlm marketing . Automatické vyhodnocovanie kampane , plánovanie a komplexný prehľad o registrovaných užívateľov analytických dát predaja .',
                'title' => 'Faberlicnassvet',
                'sub_title' => 'interná aplikácia',
                'type' => null,
            ]);
    }
}
