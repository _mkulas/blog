<?php

namespace Database\Seeders\Defaults;


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PivotsSeeder extends Seeder
{
    public function run(): void
    {
        DB::table('media_category')->insert(array(
            // Exlink
            array('media_id' => 1, 'category_id' => 2),
            array('media_id' => 1, 'category_id' => 6),
            array('media_id' => 1, 'category_id' => 3),
            // firmak
            array('media_id' => 2, 'category_id' => 2),
            array('media_id' => 2, 'category_id' => 5),
            array('media_id' => 2, 'category_id' => 3),
            // mkcore
            array('media_id' => 3, 'category_id' => 3),
            array('media_id' => 3, 'category_id' => 5),
            // roche
            array('media_id' => 4, 'category_id' => 2),
            array('media_id' => 4, 'category_id' => 6),
            // tonprint
            array('media_id' => 5, 'category_id' => 6),
            array('media_id' => 5, 'category_id' => 3),
            array('media_id' => 5, 'category_id' => 7),
            // kremik
            array('media_id' => 6, 'category_id' => 6),
            array('media_id' => 6, 'category_id' => 3),
            array('media_id' => 6, 'category_id' => 7),
            //pijeme
            array('media_id' => 7, 'category_id' => 5),
            array('media_id' => 7, 'category_id' => 7),
            //rs-tech
            array('media_id' => 8, 'category_id' => 5),
            array('media_id' => 8, 'category_id' => 4),
            //pizza-pod-agacom
            array('media_id' => 9, 'category_id' => 5),
            array('media_id' => 9, 'category_id' => 4),
            //faberlicnassvet
            array('media_id' => 10, 'category_id' => 5),
            array('media_id' => 10, 'category_id' => 4),
            array('media_id' => 10, 'category_id' => 3),

            array('media_id' => 1, 'category_id' => 1),
            array('media_id' => 2, 'category_id' => 1),
            array('media_id' => 3, 'category_id' => 1),
            array('media_id' => 4, 'category_id' => 1),
            array('media_id' => 5, 'category_id' => 1),
            array('media_id' => 6, 'category_id' => 1),
            array('media_id' => 7, 'category_id' => 1),
            array('media_id' => 8, 'category_id' => 1),
            array('media_id' => 9, 'category_id' => 1),
            array('media_id' => 10, 'category_id' => 1),
            // ...
        ));

        DB::table('content_media')->insert(array(
            // Exlink
            array('media_id' => 1, 'content_id' => 1),
            array('media_id' => 1, 'content_id' => 2),
            // firmak
            array('media_id' => 2, 'content_id' => 3),
            array('media_id' => 2, 'content_id' => 4),
            // mkcore
            array('media_id' => 3, 'content_id' => 5),
            array('media_id' => 3, 'content_id' => 6),
            // roche
            array('media_id' => 4, 'content_id' => 7),
            array('media_id' => 4, 'content_id' => 8),
            // tonprint
            array('media_id' => 5, 'content_id' => 9),
            array('media_id' => 5, 'content_id' => 10),
            // kremik
            array('media_id' => 6, 'content_id' => 11),
            array('media_id' => 6, 'content_id' => 12),
            //pijeme
            array('media_id' => 7, 'content_id' => 13),
            array('media_id' => 7, 'content_id' => 14),
            //rs-tech
            array('media_id' => 8, 'content_id' => 15),
            array('media_id' => 8, 'content_id' => 16),
            //pizza-pod-agacom
            array('media_id' => 9, 'content_id' => 17),
            array('media_id' => 9, 'content_id' => 18),
            //faberlicnassvet
            array('media_id' => 10, 'content_id' => 19),
            array('media_id' => 10, 'content_id' => 20),
            // ...
        ));
    }
}
