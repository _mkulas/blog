<?php

namespace Database\Seeders;

use Database\Seeders\Defaults\BlogSeeder;
use Database\Seeders\Defaults\CategorySeeder;
use Database\Seeders\Defaults\ContentSeeder;
use Database\Seeders\Defaults\LangSeeder;
use Database\Seeders\Defaults\MediaSeeder;
use Database\Seeders\Defaults\PivotsSeeder;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(CategorySeeder::class);
        $this->call(MediaSeeder::class);
        $this->call(LangSeeder::class);
        $this->call(BlogSeeder::class);
        $this->call(ContentSeeder::class);
        $this->call(PivotsSeeder::class);
    }
}
