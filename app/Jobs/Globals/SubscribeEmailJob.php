<?php declare(strict_types=1);

namespace App\Jobs\Globals;

use App\Mail\Globals\SubscribeEmail;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Mail;

class SubscribeEmailJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected array $details;


    public function __construct($details)
    {
        $this->details = $details;
    }


    public function handle()
    {
        Mail::to($this->details['email'])->send(new SubscribeEmail());
    }
}
