<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $uuid
 * @property int $blog_id
 * @property string $description
 * @property string $sub_description
 * @property string $title
 * @property string $sub_title
 * @property mixed $type
 */
class Content extends Model
{
    use HasFactory, UuidTrait;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'blog_id',
        'description',
        'sub_description',
        'title',
        'sub_title',
        'type'
    ];
}
