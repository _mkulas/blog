<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property mixed $uuid
 * @property mixed $type
 * @property string $alt
 * @property string $name
 * @property string $path
 * @property string $header
 */
class Media extends Model
{
    use HasFactory, UuidTrait;

    protected $hidden = ['id'];

    protected $appends = ['fullPath'];

    protected $fillable = [
        'uuid',
        'type',
        'alt',
        'name',
        'path',
        'header',
    ];

    public function category(): BelongsToMany
    {
        return $this->belongsToMany(Category::class,'media_category');
    }

    public function getFullPathAttribute(): string
    {
        return $this->path .'/'.$this->name;
    }
}
