<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $uuid
 * @property string $code
 * @property string $name
 * @property string $icon
 */
class Lang extends Model
{
    use HasFactory, UuidTrait;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'code',
        'name',
        'icon',
    ];
}
