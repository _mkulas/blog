<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

/**
 * @property mixed $uuid
 * @property string $email
 * @property mixed $activate
 */
class Subscribe extends Model
{
    use HasFactory, UuidTrait;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'email',
        'activate',
    ];
}
