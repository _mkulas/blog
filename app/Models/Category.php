<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

/**
 * @property mixed $uuid
 * @property mixed $type
 * @property string $description
 * @property string $filter
 * @property string $name
 */
class Category extends Model
{
    use HasFactory, UuidTrait;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'type',
        'filter',
        'name',
        'description',
    ];

    public function media(): BelongsToMany
    {
        return $this->belongsToMany(Media::class,'media_category');
    }
}
