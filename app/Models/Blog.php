<?php declare(strict_types=1);

namespace App\Models;

use App\Traits\UuidTrait;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\HasOne;

/**
 * @property mixed $uuid
 * @property string $title
 * @property string $description
 * @property mixed $activate
 * @property mixed $created_at
 * @property mixed $updated_at
 * @property integer $media
 * @property string $contents
 * @property mixed $lang
 * @method static create(array $array)
 */
class Blog extends Model
{
    use HasFactory, UuidTrait;

    private mixed $created_at;

    protected $hidden = ['id'];

    protected $fillable = [
        'uuid',
        'lang_id',
        'media_id',
        'category_id',
        'title',
        'description',
        'activate',
    ];


    public function media():BelongsTo
    {
        return $this->belongsTo(Media::class);
    }

    public function contents(): HasMany
    {
        return $this->hasMany(Content::class);
    }

    public function category(): BelongsTo
    {
        return $this->belongsTo(Category::class,'category_id','id');
    }

    public function lang(): HasOne
    {
        return $this->hasOne(Lang::class,'id','lang_id');
    }
}
