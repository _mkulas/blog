<?php declare(strict_types=1);

namespace App\Traits;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use Ramsey\Uuid\Uuid;

trait UuidTrait
{
    protected static function boot(): void
    {
        parent::boot();
        static::creating(static function (self $model): void {
            $model->uuid = Str::uuid();
        });
    }

    public static function findByUuid(?string $uuid): ?self
    {
        if (! $uuid) {
            return null;
        }
        return self::whereUuid($uuid)
            ->first();
    }

    public static function findOrFailByUuid($uuid): Model
    {
        return self::query()->where('uuid', $uuid)->firstOrFail();
    }

    public static function getByUuid(string $uuid): self
    {
        $model = self::whereUuid($uuid)
            ->first();

        if (! $model) {
            abort(404, 'Model does not exist');
        }

        return $model;
    }

    public function getUuidAttribute($value): string
    {
        if ($value instanceof Uuid) {
            return $value->toString();
        }
        return $value;
    }
}
