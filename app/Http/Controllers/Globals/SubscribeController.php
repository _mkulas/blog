<?php declare(strict_types=1);

namespace App\Http\Controllers\Globals;

use App\Http\Controllers\Controller;
use App\Http\Requests\Globals\StoreSubscribeRequest;
use App\Jobs\Globals\SubscribeEmailJob;
use App\Models\Subscribe;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Lang;
use function dispatch;
use function redirect;

class SubscribeController extends Controller
{
    public function store(StoreSubscribeRequest $request): RedirectResponse
    {
        if ( !$request->validated() ){
            redirect()->back()->withInput();
        }

        Subscribe::query()->create($request->validated());

        dispatch( new SubscribeEmailJob($request->validated()));

        return redirect()->back()
            ->with(['message',Lang::get('email_subscribe')]);
    }
}
