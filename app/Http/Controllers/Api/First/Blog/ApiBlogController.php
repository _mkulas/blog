<?php declare(strict_types=1);

namespace App\Http\Controllers\Api\First\Blog;

use App\Http\Controllers\Controller;
use App\Http\Requests\Api\First\Blog\IndexRequest;
use App\Http\Resources\First\Blog\BlogResource;
use App\Services\Contracts\BlogServiceContract;
use Illuminate\Http\Resources\Json\AnonymousResourceCollection;

class ApiBlogController extends Controller
{
    public function __construct(
        private BlogServiceContract $blog
    )
    {
        //
    }

    public function index(IndexRequest $request): AnonymousResourceCollection
    {
        $request->validated();
        return BlogResource::collection(
            $this->blog->getBlogs(
                $request->perPage ? (int) $request->perPage : null
            )
        );
    }
}
