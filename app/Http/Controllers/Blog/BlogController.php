<?php

namespace App\Http\Controllers\Blog;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Services\Contracts\BlogServiceContract;
use App\Services\Contracts\CategoryServiceContract;
use Illuminate\Contracts\View\View;
use function view;

class BlogController extends Controller
{
    public function __construct(
        private CategoryServiceContract $category,
        private BlogServiceContract $blog
    ){
        //
    }


    public function index(): View
    {
        return view('blog.index',[
            'blog' => true,
            'categories' => $this->category->categoryBlog(),
            'blog_posts' => $this->blog->getBlogs(),
        ]);
    }


    public function show(Blog $blog): View
    {
        return view('blog.pages.product',
            ['blog' => $blog]
        );
    }
}
