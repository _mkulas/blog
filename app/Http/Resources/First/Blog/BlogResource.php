<?php declare(strict_types=1);

namespace App\Http\Resources\First\Blog;

use App\Models\Blog;
use Illuminate\Http\Resources\Json\JsonResource;
use JetBrains\PhpStorm\ArrayShape;
use JetBrains\PhpStorm\Pure;

class BlogResource extends JsonResource
{
    private Blog $blog;

    #[Pure]
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->blog = $resource;
    }


    #[ArrayShape(['blog' => "array"])]
    public function toArray($request): array
    {
        return [
            'blog' => [
                'uuid' => $this->blog->uuid,
                'title' => $this->blog->title,
                'description' => $this->blog->description,
                'activate' => $this->blog->activate ,
                'created_at' => $this->blog->created_at ,
                'updated_at' => $this->blog->updated_at,
                'lang' => $this->blog->lang,
                'contents' => $this->blog->contents,
                'media' => $this->blog->media
            ],
        ];
    }
}
