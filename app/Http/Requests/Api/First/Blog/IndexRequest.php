<?php declare(strict_types=1);

namespace App\Http\Requests\Api\First\Blog;

use Illuminate\Foundation\Http\FormRequest;
use JetBrains\PhpStorm\ArrayShape;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

/**
 * @property integer $perPage
 */
class IndexRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }


    #[ArrayShape(['perPage' => "integer"])]
    public function rules(): array
    {
        return [
            'perPage' => [
                'integer',
            ]
        ];
    }

    #[ArrayShape(['perPage.integer' => "string"])]
    public function messages(): array
    {
        return [
            'perPage.integer' => 'Not format'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }
}
