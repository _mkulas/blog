<?php declare(strict_types=1);

namespace App\Http\Requests\Globals;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Lang;
use JetBrains\PhpStorm\ArrayShape;

class StoreSubscribeRequest extends FormRequest
{
    public function authorize(): bool
    {
        return true;
    }


    #[ArrayShape(['email' => "string"])]
    public function rules(): array
    {
        return [
            'email' => 'email|unique:subscribes'
        ];
    }


    #[ArrayShape(['email.email' => "string", 'email.unique' => "string"])]
    public function messages(): array
    {
        return [
            'email.email' => Lang::get('portfolio.email'),
            'email.unique' => Lang::get('portfolio.unique')
        ];
    }
}
