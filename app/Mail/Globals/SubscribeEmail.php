<?php declare(strict_types=1);

namespace App\Mail\Globals;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class SubscribeEmail extends Mailable
{
    use Queueable, SerializesModels;

    public function build(): SubscribeEmail
    {
        return $this->view('email.subscribe');
    }
}
