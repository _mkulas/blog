<?php

namespace App\Providers;

use App\Services\BlogService;
use App\Services\CategoryService;
use App\Services\Contracts\BlogServiceContract;
use App\Services\Contracts\CategoryServiceContract;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function register()
    {
        //
    }

    public array $singletons = [
        CategoryServiceContract::class => CategoryService::class,
        BlogServiceContract::class => BlogService::class
    ];

    public function boot()
    {
        //
    }
}
