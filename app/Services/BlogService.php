<?php declare(strict_types=1);

namespace App\Services;

use App\Models\Blog;
use App\Services\Contracts\BlogServiceContract;
use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Builder;

final class BlogService implements BlogServiceContract
{
    public function getBlogs(?int $perPage = null): LengthAwarePaginator
    {
        return $this->blog()->paginate($perPage ?? 6);
    }

    private function blog(): Builder
    {
        return Blog::query()
            ->with('media','contents','lang','category')
            ->where('lang_id',config('app.lang'))
            ->orderBy('id','desc');
    }
}
