<?php declare(strict_types=1);

namespace App\Services\Contracts;

use Illuminate\Contracts\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\Collection;

interface BlogServiceContract
{
    public function getBlogs(?int $perPage): LengthAwarePaginator;
}
