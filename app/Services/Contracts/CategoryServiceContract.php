<?php declare(strict_types=1);

namespace App\Services\Contracts;

use Illuminate\Database\Eloquent\Collection;

interface CategoryServiceContract
{
    public function categoryBlog(): Collection|array;
}
