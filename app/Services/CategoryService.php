<?php declare(strict_types=1);

namespace App\Services;

use App\Models\Category;
use App\Services\Contracts\CategoryServiceContract;
use Illuminate\Database\Eloquent\Collection;

final class CategoryService implements CategoryServiceContract
{
    public function categoryBlog(): Collection|array
    {
        return Category::query()->where('type','blog')->get();
    }
}
