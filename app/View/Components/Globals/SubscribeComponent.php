<?php declare(strict_types=1);

namespace App\View\Components\Globals;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use function view;

class SubscribeComponent extends Component
{
    public function render(): View
    {
        return view('components.globals.subscribe-component');
    }
}
