<?php declare(strict_types=1);

namespace App\View\Components\Globals;

use Illuminate\Contracts\View\View;
use Illuminate\View\Component;
use function view;

class MenuComponent extends Component
{
    public mixed $blog;

    public function __construct($blog = null)
    {
        $this->blog = $blog;
    }

    public function render(): View
    {
        return view('components.globals.menu-component');
    }
}
