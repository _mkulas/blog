<!-- Footer -->
<div class="vg-footer">
    <div class="container">
        <div class="row justify-content-center mt-3">
            <div class="col-12 mb-3">
                <h3 class="fw-normal text-center">{{ __('portfolio.subscribes') }}</h3>
            </div>
            <div class="col-lg-6">
                <form class="mb-3" action="{{ url('subscribe') }}" method="post">
                    @csrf
                    <div class="input-group">
                        <label for="email"></label>
                        <input type="text" id="email" class="form-control" name="email" placeholder="{{ __('portfolio.email') }}">
                        <input type="submit" class="btn btn-theme no-shadow" value="{{ __('portfolio.subscribe') }}">
                    </div>
                </form>
            </div>
            <div class="col-12">
                <p class="text-center mb-0 mt-4">{{ __('portfolio.copyright') }}</p>
            </div>
        </div>
    </div>
</div>
<!-- End footer -->
