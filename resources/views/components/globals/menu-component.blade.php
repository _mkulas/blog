<!-- Navbar -->
<div class="navbar navbar-expand-lg navbar-dark {{ $blog ? 'bg-dark' : 'sticky" data-offset="500' }}">
    <div class="container">
        <button class="navbar-toggler" data-toggle="collapse" data-target="#main-navbar" aria-expanded="true">
            <span class="ti-menu"></span>
        </button>
        <div class="collapse navbar-collapse" id="main-navbar">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="{{ url('/') }}" class="nav-link" data-animate="scrolling">{{ __('portfolio.home') }}</a>
                </li>
            </ul>
            <ul class="nav ml-auto">
                <li class="nav-item">
                    <button class="btn btn-fab btn-theme no-shadow">{{ config('app.locale') }}</button>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- End Navbar -->
