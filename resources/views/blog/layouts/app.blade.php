<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title></title>
    <link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
    <link rel="stylesheet" type="text/css" href="{{ asset('V1/css/app.css') }}">
</head>
<body class="theme-red">

    @yield('content')

    <script src="{{ asset('V1/js/jquery-3.5.1.min.js') }}"></script>
    <script src="{{ asset('V1/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('V1/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('V1/vendor/isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('V1/vendor/nice-select/js/jquery.nice-select.min.js') }}"></script>
    <script src="{{ asset('V1/vendor/wow/wow.min.js') }}"></script>
    <script src="{{ asset('V1/js/topbar-virtual.js') }}"></script>
</body>
</html>
