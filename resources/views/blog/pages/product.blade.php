@extends('blog.layouts.app')
@section('content')
    <x-globals.menu-component blog="blog"></x-globals.menu-component>
    <div class="container">
        <div class="mt-5 text-center">
            <h2 class="text-dark">{{ __('portfolio.message') }} </h2>
        </div>
    </div>
@endsection
