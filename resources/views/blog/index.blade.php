@extends('blog.layouts.app')
@section('content')
    <!-- Navbar -->
    <!-- navbar -->
    <div id="app">
        <div class="vg-page page-blog">
            <div class="container">
                <div class="row widget-grid">
                    <div class="col-lg-8"></div>
                    <div class="col-lg-4">
                        <div class="d-flex py-2 mx-n2">
                            <div class="input-group px-2">
                                <select id="l_category" class="vg-select">
                                    <option value="">{{ __('portfolio.category') }}</option>
                                    @foreach($categories as $category)
                                        <option
                                            value="{{ $category->filter }}">{{ __('portfolio.' .$category->name) }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="input-group px-2">
                                <select id="l_sort" class="vg-select">
                                    <option value="">{{ __('portfolio.sort_by') }}</option>
                                    <option value="cat-1">....</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row post-grid">
                    @foreach($blog_posts as $post)
                        <div class="col-md-6 col-lg-4">
                            <div class="card">
                                <div class="img-place">
                                    <img src="{{ asset('/img/'.$post->media->fullPath) }}"
                                         alt="{{ $post->media->alt }}">
                                </div>
                                <div class="caption">
                                    <a href="javascript:void(0)" class="post-category">{{ $post->title }}</a>
                                    <a href="{{ route('blog.show',[$post]) }}" class="post-title">{{ $post->description }}</a>
                                    <span class="post-date"><span
                                            class="sr-only">{{ __('portfolio.published') }}</span> {{ $post->created_at->format('d M Y') }}</span>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    <div class="col-12 py-3">
                        <div class="text-center m-3">
                            <p class="post-date">  {{ __('portfolio.page') . $blog_posts->currentPage() . __('portfolio.of') . $blog_posts->lastPage() }}</p>
                        </div>
                        @if ($blog_posts->hasPages())
                            <ul class="pagination justify-content-center">
                                @if ($blog_posts->onFirstPage())
                                    <li class="page-item disabled">
                                        <a href="{{ $blog_posts->previousPageUrl() }}"
                                           class="page-link" rel="prev">{{ __('Prev') }}</a>
                                    </li>
                                @else
                                    <li class="page-item">
                                        <a href="{{ $blog_posts->previousPageUrl() }}"
                                           class="page-link" rel="prev">{{ __('Prev') }}</a></li>
                                @endif
                                @if ($blog_posts->hasMorePages())
                                    <li class="page-item active">
                                        <a class="page-link"
                                           href="{{ $blog_posts->nextPageUrl() }}"
                                           rel="next">{{ __('Next') }}</a></li>
                                @else
                                    <li class="page-item disabled">
                                        <a href="" class="page-link" rel="prev">{{ __('Next') }}</a>
                                    </li>
                                @endif
                            </ul>
                        @endif
                    </div>
                    <x-globals.subscribe-component></x-globals.subscribe-component>
                </div>
            </div>
        </div>
    </div>
    <label for="l_search"></label>
    <label for="l_category"></label>
    <label for="l_sort"></label>
@endsection

