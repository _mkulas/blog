Štruktúra aplikácie 
    
        app 
        - Http
        -- Controller 
        ---Api 
        ---- First - ( verzia api )
        ------ Blog - ( časť api )
                
        - Blog ( časť aplikácie )
        -- Globals - ( globálne používané )
    
        Jobs
        - Globals ( job pre globálne používanie - subscribe )
    
        Mail
        - Globals ( mail pre globálne použivanie - subscribe )
    
        Services 
        - Contracts
        
        View
        -Components
        -- Globals ( globálne použivané componenty )


Ukážka štruktúrovania a používania laravelu na úrovni BACKENDU

K ukážke treba brať do úvahy že sa kodí ako súčasť večšej aplikácie .

Je kodená spôsobom microservice .
    
Databazová štruktúra je navrhnutá tak aby BLOG mohol byť multyjazyčný 
    
Z obsahom ktorý je rozdelený do neobmedzeného počtu -
takisto multijazyčný a má neobmedzený počet medii
pre každú časť obsahu . 

Jedná sa ukážku BACKENDU . Štruktúry kódu ,
používania laravelu a jeho súčasti ale aj custom časti ktoré vykonajú
automatický proces . 
( napr v seederi namiesto insertu použitá create 
v ktorej sa automaticky vygeneruje uuid . 

Frontend: ( len základný )

Výpis homepage blogu a 
Poslanie emailu z frontendu pre subscribe 
    
        

Použité:

    Laravel9
    PHP8.1
    node 16.14.0
    npm 8.3.1


Start app

rename .env.example to .env

    cp .env.example .env

    config email smtp 
  
    MAIL_MAILER=smtp
    MAIL_HOST=
    MAIL_PORT=
    MAIL_USERNAME=
    MAIL_PASSWORD=
    MAIL_ENCRYPTION=
    MAIL_FROM_ADDRESS=
    MAIL_FROM_NAME="

config db

    DB_CONNECTION=
    DB_HOST=
    DB_PORT=
    DB_DATABASE=
    DB_USERNAME=
    DB_PASSWORD=

    npm install 
    npm run dev 

run :
    
    php artisan migrate:fresh --seed
    php artisan serve 

or

    http://localhost/projectname/public


Ukážka použitia: BACKENDU !

      + Odošle email pomovou job
      + Použitie trait pre modely
      + Automatické generovanie dat do modelu pri použití metódy create
      + Seeder
      + Migracie 
      + Microservice 
      + Laravel Request validácia
      + Vzťahy medzi tabuľkami v modeloch aj ( opačným smerom ) 
      + Vlastné attributy
      + Laravel componenty
      + Route ( resources -> only , POST , GET , NAME ... )
      + Ukážka dynamického vyťahovania prekladu z default laravelu na zaklade key .
      + Resource pre api vátane relations ukážka na url nižšie

http://127.0.0.1:8000/api/v1/blog

Ak chceme nastaviť iný limit ako default použijeme parameter perPage

http://127.0.0.1:8000/api/v1/blog?perPage=1

      + pouzitie a doplnenie vlastneho config parametru pre podmienkovanie a výpis blade ( v ukážke nedynamické )
      + Návrh databázovej štruktúry


Ukážka návrhu package pre laravel: + verziovanie v gite

https://gitlab.com/_mkulas/mkulas-mail.git

Odosielanie emailov s prílohami

https://gitlab.com/_mkulas/odyzeo/-/tree/master
