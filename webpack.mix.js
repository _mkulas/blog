const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/js/app.js', 'public/V1/js')
    .copy('resources/js/V1/','public/V1/js')
    .copy('resources/js/vendor/', 'public/V1/vendor')
    .sass('resources/sass/V1/app.scss', 'public/V1/css')
    .sourceMaps();
