<?php declare(strict_types=1);

return  [
    'search' => 'Hľadať',
    'of' => ' z ',
    'page' => 'Stránka ',
    'category' => 'Kategórie',
    'sort_by' => 'Triediť podľa',
    'email' => 'Email pre odoberanie noviniek ...',
    'subscribe' => 'Odoberať',
    'subscribes' => 'Odoberať novinky',
    'home' => 'Home',
    'projekty' => 'Projekty',
    'message' => 'Práve pripravujem.',
    'copyright' => 'Všetky práva vyhradené',//'Copyright All right reserved',
    'published' => 'Zverejnené dňa',
    'email_subscribe' => 'Ahoj . Práve si sa prihlásil/a na odber noviek z môjho blogu.'
];
