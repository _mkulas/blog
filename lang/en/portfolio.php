<?php declare(strict_types=1);

return  [
    'search' => 'Search',
    'of' => ' of ',
    'page' => 'Page ',
    'category' => 'Category',
    'sort_by' => 'Sort by',
    'email' => 'News subscription ...',
    'subscribe' => 'Subscribe',
    'subscribes' => 'Subscribe to Newsletter',
    'home' => 'Home',
    'projekty' => 'Projects',
    'message' => 'Preparing',
    'copyright' => 'Copyright All right reserved',
    'published' => 'Published',
    'email_subscribe' => 'Hi . You have just subscribed to news from my blog.'
];
